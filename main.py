#!/usr/bin/env python3
import os
import sys
PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__),  os.pardir))
sys.path.append(PROJECT_ROOT)
from utils import *

#variables and settings
n_initials = 1
iterations = 200+ n_initials
n_events = 1000000 #number of events; ideally over= 1mil
real = 'on' #if real = 'on', tune on the real experimental data
noise = 'gaussian' #assume no (statistical) noise)
normalize_y = True #(should the mean be normalized by the chisquare values before prediction)

seed = 7 #set a seed for reproducibility
rng = check_random_state(seed)

params_true = params_true_Monash #closure tune 'true' values
paramstobetuned = [0,1,2,3] 
#turn the paramstobetuned into a shape that works with the optimisation:
paramrange_tot = {0:[0.01,1.],1:[0.2,2.] ,2:[0.01,2.] ,3:[0.01,2.] }
paramrange = {}
for elm in paramstobetuned:
	paramrange[elm] = paramrange_tot[elm]
paramrangelist = [tuple(elm) for elm in paramrange.values()]
space = Space(normalize_dimensions(paramrangelist))
n_dims = space.transformed_n_dims



#the observables which contribute to the chisquare
observables= np.array(['/ALEPH_2004_S5765862/d54-x01-y01','/ALEPH_1991_S2435284/d01-x01-y01','/ALEPH_2001_S4656318/d01-x01-y02', '/ALEPH_1999_S4193598/d01-x01-y01'])


#################build estimator with kernel###########################################
'''
#either set up a base estimator specifically or 'cook' one
kwargs = {'random_state': rng.randint(0, np.iinfo(np.int32).max),'noise':noise}

cov_amplitude = ConstantKernel(1.0, (0.01, 1000.0))

other_kernel = Matern(
                length_scale=np.ones(n_dims),
                length_scale_bounds=[(0.01, 10.)] * n_dims, nu=2.5)
base_estimator = GPR(
            kernel=cov_amplitude * other_kernel,
            normalize_y=normalize_y, noise=noise,
            n_restarts_optimizer=10)
base_estimator.set_params(**kwargs)
'''
kwargs = {'random_state': rng.randint(0, np.iinfo(np.int32).max)}
base_estimator = cook_estimator('GP', space, **kwargs)
	
############for saving the results############
if real == 'on':
	res_each_it_file = 'results/results_each_it_to_real.txt'
	res_full_file  = 'results/results_full_to_real.txt'
	timefile = 'results/time_each_it_to_real.txt'
else:
	res_each_it_file = 'results/results_each_it_skopt.txt'
	res_full_file  = 'results/results_full_skopt.txt'
	timefile = 'results/time_each_it_skopt.txt'


with open(res_each_it_file, 'a+') as file:
		now = datetime.now()
		file.write('\n# '+str(now) + '\t Params true '+ str(params_true) +'\t chisquare of params '+str(paramstobetuned)+ '\t in range '+ str(paramrange)+'\t#for number events = '+str(n_events)+' and observables '+ str(observables)+' seed '+ str(seed)+ ' noise '+ str(noise)+ ' real: '+ str(real) + ' normalizey = ' + str(normalize_y)+ '\n #baseest ' + str(base_estimator)+ '\n')
		file.write('#run number \t call val \t param1 \t  param2 \t param3 \t param4 \t \n')

	
with open(timefile, 'a+') as file:
		now = datetime.now()
		file.write('\n# '+str(now) + '\t Params true '+ str(params_true) +' chisquare of params '+str(paramstobetuned)+ ' in range '+ str(paramrange)+' for number events = '+str(n_events)+' and observables '+ str(observables)+' seed ' + str(seed) + ' normalizey = ' + str(normalize_y)+'\n')
		file.write('t_wall'+ '\t t_cpu \n')

with open(res_full_file, 'a+') as file:
		now = datetime.now()
		file.write('\n '+str(now))
		file.write('\n Params true '+ str(params_true))
		file.write('\n chisquare of params '+str(paramstobetuned)+ '\t in range '+ str(paramrange))
		file.write('\n with observables '+ str(observables))
		file.write('\n for number events = '+str(n_events)+' with seed '+ str(seed)+ ' and noise '+ str(noise)+ ' ,real: '+ str(real) + ' ,normalizey = ' + str(normalize_y))
		file.write('\n #baseest ' + str(base_estimator)+ '\n')
		if params_true is not None:
			file.write('\n Closure test for true values ' + str(params_true))
###################################################################
#now optimise

#wrapper for the callbackfunctions (for writing down results) and the function that is optimised
callwrapres = lambda x: callres(x, res_each_it_file)
callwraptime = lambda x : calltime(x, timefile)
fwrap = lambda x: f(x, real, n_events, observables, 'optimisation.yoda')
'''	
res =  base_minimize(fwrap, space, base_estimator=base_estimator,
        acq_func='EI',
        xi=0.01, kappa=1.96, acq_optimizer='auto', n_calls=iterations,
        n_points=int(1e6), n_random_starts=None,
        n_initial_points=n_initials,
        initial_point_generator='random',
        n_restarts_optimizer=10,
        x0=None, y0=None, random_state=rng, verbose=False,
        callback=[callwrapres, callwraptime], n_jobs=1, model_queue_size=None)

'''
res = create_result_object('results/written_result.txt', space, rng)

########call one last time at the predicted optimum ###########################

ybest, xbest, maxacq = predict_after(res, n_points = int(1e6),n_spray = int(1e4), rng=rng)
print(ybest, xbest, maxacq)
'''
chi_final = f(xbest, real, n_events, observables, 'chi_final.yoda')

#########uncertainty estimation ##################################
samplesperbatch = 2
batchnum = 2
replicas_samp, chisqu_pred_repl_samp = uncertainty_sampling(res, n_points = int(1e3), n_spray = int(1e3), samplesperbatch = samplesperbatch, batchnum = batchnum,xbestpred = xbest)
print(replicas_samp)
t.append(time())
chisqu_real_repl_samp = []
for c, elm in enumerate(replicas_samp):
	saveas = 'samp_repl_'+str(c)+'.yoda'
	chisqu_real_repl_samp.append(f(elm, real, n_events,observables, saveas))	
t.append(time())	
################now write down results###############################################################
write_res(res, name = 'results/resskopt.txt')
write_res_full(res_full_file, res,  ybest, xbest, maxacq, replicas_samp,chisqu_pred_repl_samp, chisqu_real_repl_samp, samplesperbatch*batchnum, params_true)

#####plots#################################################################
call(["python3","utils/yoda_envelops.py","-o", "Analyses/samp_envelop.yoda","Analyses/samp*"])
#python3 utils/yoda_envelops.py -o Analyses/samp_envelop.yoda Analyses/samp*
call(["rivet-mkhtml","Analyses/samp_envelop.yoda:ErrorBands=1:ErrorBandOpacity=0.6","-o", "plots/samp_error_envelops"])	
#rivet-mkhtml Analyses/samp_envelop.yoda:ErrorBands=1:ErrorBandOpacity=0.6 -o plots/samp_error_envelops


plot_convergence_chisqu(res)
plt.savefig('plots/chisquconvergence.pdf', bbox_inches='tight')

plot_convergence_each_param (res, params_true)
plt.savefig('plots/paramchisquconvergence.pdf', bbox_inches = 'tight')


plot_cpu(timefile, iterations-n_initials)
plt.savefig('plots/cpu.pdf', bbox_inches='tight')


plot_objective(res, levels=10, n_points=100, n_samples=250, size=2,
                       zscale='linear', dimensions=None, sample_source='random',
                       minimum='expected_minimum', n_minimum_search=None, plot_dims=None,
                       show_points=True, cmap='viridis_r')
plt.savefig('plots/plotobjective.pdf', bbox_inches='tight')

plot_evaluations(res, bins=20, dimensions=None,plot_dims=None)
plt.savefig('plots/plotevaluations.pdf', bbox_inches='tight')
'''
plot_objective_acq(res, levels=10, n_points=100, n_samples=250, size=2,
                       zscale='linear', dimensions=None, sample_source='random',
                       minimum=maxacq, n_minimum_search=None, plot_dims=None,
                       show_points=True, cmap='viridis')
plt.savefig('plots/plotobjectiveacq.pdf', bbox_inches='tight')

