sigmaMonash = 0.335 #21
bLundMonash = 0.98 #42
rCMonash = 1.32 #46
rBMonash = 0.855#47

sigmaDP64 = 0.36
bLundDP64 = 0.58
rCDP64 = 1
rBDP64 = 1

sigmaT64 = 0.329
bLundT64 = 1.65
rCT64 = 1.42
rBT64 = 0.975
params_true_Monash = {0: sigmaMonash, 1: bLundMonash, 2: rCMonash, 3: rBMonash}
params_true_DP64 = {0: sigmaDP64, 1: bLundDP64, 2: rCDP64, 3: rBDP64}
params_true_T64 = {0: sigmaT64, 1: bLundT64, 2: rCT64, 3: rBT64}
params_true_list = [params_true_Monash, params_true_DP64, params_true_T64]

fontsize = 20
legendsize = 14
labelsize = 14
