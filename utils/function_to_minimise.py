import os
import sys
PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__),  os.pardir))
sys.path.append(PROJECT_ROOT)
from utils import *


def f(p, real, n_events,observables, saveas = 'default.yoda'):
    '''
    p: list of the parameter values of the parameters that are tuned
    real: if 'on', tune to real data; if 'off' tune to closure test data
    observables: the names of the observables that contribute to the chisquare
    saveas: name of yoda file where the event generator results are stored
    '''

    #read histogram objects from the reference data
    if real == 'on':
    	trueh = {}
    	for name in observables:
    		trueh[name] = yoda.read('/home/k.schmitt/rivet/Rivet-3.1.6/analyses/pluginLEP/'+ name.split('/')[1]+'.yoda')
		#here we use the LEP yoda files from rivet for the experimental data
    else:
        trueh = yoda.read('utils/Closuretest.yoda')


    trueheights = {} #heights of the 'true' reference data
    trueyerrs = {} #errors of the 'true' reference data
    dof = {} # degrees of freedom per each histogram
    doftot = 0 # total degrees of freedom
    for name in observables:
        try:
            if real == 'on': truehisto=trueh[name]['/REF'+name]
            else: truehisto=trueh[name]

        except:
            print('error {} not found'.format(name))
            continue

        if real == 'on':
       	    #the real reference data are YODA_SCATTER2D_V2 objects
            trueheightsname = np.array(truehisto.yVals()) 
            trueyerrsname = np.array(truehisto.yErrAvgs())
        else:
            #the reference data produced by the event generator are YODA_HISTO1D_V2 objects
            trueheightsname = np.array(truehisto.heights())
            trueyerrsname = np.array(truehisto.yErrs())
            #only for the closure test, set a min percentage of 1% for the error
            for idx, yerr in enumerate(trueyerrsname):
                if yerr < 0.01*trueheightsname[idx]:
                    trueyerrsname[idx] = 0.01*trueheightsname[idx]

        trueheights[name] = trueheightsname
        trueyerrs[name] = trueyerrsname
        dof[name] = len(trueheightsname)
        doftot += len(trueheightsname)

    #call the eventgenerator with the parameters p
    call_sherpa(p,observables, saveas = saveas, n_events= n_events)
    
    h = yoda.read('Analyses/'+saveas)
    chi2 = 0
    for name in observables:
            try:
                histo=h[name]
            except:
                print('error {} not found'.format(name))
                continue
            heights = np.array(histo.heights())
            yerrs = np.array(histo.yErrs())
            for idx, yerr in enumerate(yerrs):
                if yerr < 0.01*heights[idx]:
                    yerrs[idx] = 0.01*heights[idx]

            chi2 += chisqu(trueheights[name], heights, trueyerrs[name], yerrs)
          
    chi2 = chi2/doftot #divide by degrees of freedom
    return chi2
