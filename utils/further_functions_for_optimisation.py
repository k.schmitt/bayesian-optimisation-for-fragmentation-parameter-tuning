from .imports import *

################################################################################
def call_sherpa(p,  namesofinterest, saveas = 'Analysistest.yoda', n_events = 10000, rseed = 0):
    sigma =p[0]
    bLund = p[1]
    rC=p[2]
    rB = p[3]


    l = []
    for elm in namesofinterest:
        elm = elm.split('/')
        try:
            if elm[1] not in l:
                l.append(elm[1])
        except:continue
    l = ' '.join(l)

    with open('utils/Run.txt', 'r') as vorlage:
        with open('Run.dat', 'w+') as file:
            for line in vorlage:
                if 'EVENTS' in line:
                    file.write('EVENTS {0};\n'.format(n_events))
                elif 'PARJ' in line:
                    file.write('PARJ(21) {0}; PARJ(42) {1}; PARJ(46) {2}; PARJ(47) {3}; MSTJ(11) 5\n'.format(sigma, bLund, rC, rB))
                elif '-a' in line:
                    file.write('-a '+str(l)+' \n')
                else:
                    file.write(line)
    call(["/home/k.schmitt/sherpa/SHERPA-MC-2.2.12/bin/Sherpa"])
    call(["gzip", "-dk", "Analyses/Analysis.yoda.gz"])
    call(["mv", "Analyses/Analysis.yoda", 'Analyses/'+saveas])
    return 0


################################################################################
################################################################################
def chisqu(trueheights, heights, trueyerrs, yerrs):
    #sanity test1: test the shapes
    #shapes = np.asarray([X.shape, Y.shape, sigX.shape, sigY.shape])
    #if (np.all(shapes == shapes[0]))==False:
     #   print('wrong shapes, no chisqu possible')

    truezeroidces = np.where(trueheights == 0)[0]
    zeroidces = np.where(trueheights == 0)[0]
    bothzeroidces = list(set(truezeroidces).intersection(zeroidces))


    X= np.delete(trueheights, bothzeroidces)
    Y = np.delete(heights, bothzeroidces)
    sigX = np.delete(trueyerrs, bothzeroidces)
    sigY = np.delete(yerrs, bothzeroidces)

    if len(X) == 0:
        print('chisquare not possible, no overlap')
        return 0

    else:
        chiarray = (X-Y)**2/(sigX**2 + sigY**2)
        chisquare = np.sum(chiarray)

        #sanity test 2
        #if chisquare < 0:
        #   print('error chisquare smaller 0', chisquare)
        return chisquare

#the following functions are not used anymore. they were helpful for writing an own version of BO
################################################################################
def acquisition(mu_pred, std_pred, y_opt, acqu_function = 'ei'):
#X, Xsamples, model, acqu_function = 'ei', y = None):

    #we need y only for 'ei as stolen from spearmint
    #or use expected improvement with best surrogate score found so far

    if acqu_function == 'ei':
        xi = 0.
        #print(y_opt, xi)
        values = np.zeros_like(mu_pred)
        mask = std_pred > 0
        improve =  mu_pred[mask]  -y_opt - xi
        scaled = improve / std_pred[mask]
        cdf = norm.cdf(scaled)
        pdf = norm.pdf(scaled)
        exploit = improve * cdf
        explore = std_pred[mask] * pdf
        values[mask] = exploit + explore
        probs2 = (mu_pred-y_opt-xi) *  sps.norm.cdf((mu_pred-y_opt-xi)/(std_pred+1E-9)) + std_pred * sps.norm.pdf((mu_pred-y_opt-xi)/(std_pred+1E-9))
        probs = values[mask]
        #print('improve', (mu_pred-y_opt)[:10], improve[:10])
        #print('scaled', ((mu_pred-y_opt)/std_pred)[:10], scaled[:10])
        #print('cdfs',  sps.norm.cdf((mu_pred-y_opt-xi)/(std_pred+1E-9))[:10], cdf[:10])
        #print('the probs\n', probs[:10],'\n no2', probs2[:10])



    else: #just to make sure, is unnecessary since aqu_function is optional parameter zum Übergeben
        print('error - no acquisition function specified')
    return probs

################################################################################

def opt_acquisition(y_opt, X_opt,model, x_min, x_max):#,acqu_function):
    spacedim = len(x_min)

    #random search within parameter space to obtain maximum of acquisition function
    #Xsamples = x_min + (x_max-x_min)* np.random.random(1000*spacedim).reshape(1000,spacedim)


    #sobol search within parameter space to obtain maximum of acquisition function
    sampler = qmc.Sobol(d=spacedim, scramble=True)
    sample = sampler.random_base2(m=17)#2^m points -> 1024
    Xsamples = qmc.scale(sample, x_min, x_max).reshape(-1,spacedim)


    #Add some extra candidates around the best so far (a useful hack) 
    num_spray = 100
    spray_std = 1e-2*(x_max - x_min)
    spray_points = np.random.randn(num_spray, spacedim)*spray_std + X_opt
    Xsamples =  np.vstack((Xsamples, spray_points))
    #spray_points = np.minimum(np.maximum(spray_points,0.0),1.0)


    mu_pred, std_pred =model.predict(Xsamples, return_std = True)
    scores = acquisition(mu_pred, std_pred, y_opt)#,acqu_function)
    ix = np.argmax(scores)
    return Xsamples[ix]

def pred_max(X_opt,model, x_min, x_max):

    spacedim = len(x_min)

    #random search within parameter space to obtain maximum of gaussian process model
    #Xsamples = x_min + (x_max-x_min)* np.random.random(1000*spacedim).reshape(1000,spacedim)

    #sobol search within parameter space to obtain maximum of gaussian process model
    sampler = qmc.Sobol(d=spacedim, scramble=True)
    sample = sampler.random_base2(m=17)#2^m points -> 1024
    Xsamples = qmc.scale(sample, x_min, x_max).reshape(-1,spacedim)

    #Add some extra candidates around the best so far (a useful hack) 
    num_spray = 100
    spray_std = 1e-2*(x_max - x_min)
    spray_points = np.random.randn(num_spray, spacedim)*spray_std + X_opt
    Xsamples =  np.vstack((Xsamples, spray_points))
    #spray_points = np.minimum(np.maximum(spray_points,0.0),1.0)


    mu_pred, std_pred =model.predict(Xsamples, return_std = True)
    ix = np.argmax(mu_pred)
    return Xsamples[ix]



