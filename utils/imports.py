import scipy.integrate as integrate
import sys
import pandas as pd
import os
import subprocess
from subprocess import call
import yoda
import math
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
import warnings
import joblib
from joblib import Parallel, delayed
import scipy
from scipy.spatial.distance import cdist, pdist
import sys
from time import process_time, time
from datetime import datetime
from matplotlib.colors import LogNorm, CenteredNorm
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import scipy.stats as sps
from scipy.stats import norm, qmc
from sklearn.utils import check_random_state
import skopt
from skopt.space import Space, Categorical, Integer, Real, Dimension

from skopt.optimizer.base import base_minimize
from skopt.utils import cook_estimator, normalize_dimensions, create_result
from skopt.space import Space, Categorical, Integer, Real, Dimension
from skopt.acquisition import _gaussian_acquisition
from skopt.plots import _evenly_sample, plot_objective_2D, plot_objective, plot_histogram, plot_evaluations, plot_convergence, _format_scatter_plot_axes,partial_dependence_2D, _evaluate_min_params,_map_categories, plot_gaussian_process

from skopt.learning import GaussianProcessRegressor #as GPR
from skopt.learning.gaussian_process.kernels import Matern #as Maternskoptlearning
from skopt.learning.gaussian_process.kernels import ConstantKernel #as ConstantKernelskoptlearning
from skopt.learning.gaussian_process.kernels import WhiteKernel #as WhiteKernelskoptlearning
#careful to import the right kernels, from skopt not from sklearn directly
