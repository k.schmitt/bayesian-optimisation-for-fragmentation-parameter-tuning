from .imports import *
from skopt.plots import _evenly_sample, plot_objective_2D, plot_objective, plot_histogram, plot_evaluations, plot_convergence, _format_scatter_plot_axes,partial_dependence_2D, _evaluate_min_params,_map_categories, plot_gaussian_process
from skopt.acquisition import _gaussian_acquisition



#################################################################################
#plot the wallclock time and CPU time of the optimisation
def plot_cpu (name, n_times):
    n_rows = sum(1 for row in open(name, 'r'))
    df = pd.read_csv(name, sep="\t", header = None, comment = '#', skiprows = range(1, n_rows-n_times))
    print(df)
    timereal = np.array(df[0][:].astype(float))
    timecpu = np.array(df[1][:].astype(float))
    timereal2 = timereal[1:]- timereal[:-1]
    timereal2 = timereal2/60
    timecpu2 = timecpu[1:]-timecpu[:-1]
    x = np.arange(len(timereal))
    print(x, timereal2)
    fig, ax = plt.subplots(1,2, figsize=(20,5))#, gridspec_kw={'width_ratios': [1, 2]})
    ax[0].plot(x[1:], timereal2, color = 'xkcd:deep aqua', label ='time taken for call $n$')
    ax[0].set_xlabel('call number $n$', fontsize=fontsize)
    ax[0].set_ylabel(' real time $t$ in min', fontsize=fontsize)
    ax[1].plot(x[1:], timecpu2, color = 'xkcd:deep aqua', label ='CPU time taken for call $n$')
    ax[1].set_xlabel('call number $n$', fontsize=fontsize)
    ax[1].set_ylabel('CPU time $t_{{CPU}}$ in s', fontsize=fontsize)
    ax[0].xaxis.set_major_locator(MultipleLocator(25))
    ax[0].xaxis.set_minor_locator(MultipleLocator(5))    
    ax[0].yaxis.set_major_locator(MultipleLocator(0.5))
    ax[0].yaxis.set_minor_locator(MultipleLocator(0.1))
    ax[1].xaxis.set_major_locator(MultipleLocator(25))
    ax[1].xaxis.set_minor_locator(MultipleLocator(5))    
    ax[1].yaxis.set_major_locator(MultipleLocator(2))
    ax[1].yaxis.set_minor_locator(MultipleLocator(0.5))
    ax[0].tick_params(axis='both', which='major', labelsize=labelsize)
    ax[1].tick_params(axis='both', which='major', labelsize=labelsize)
    ax[0].grid(alpha = 0.5)
    ax[1].grid(alpha = 0.5)


#################################################################################
#plot the chisquare value in each iterations
def plot_convergence_chisqu (res):
    chisquare = res.func_vals
    x = np.arange(len(chisquare))
    
    fig, ax = plt.subplots( figsize=(10,5))
    ax.plot(x,chisquare,color = 'xkcd:deep aqua', label ='$\chi^2$ value in call $n$')
    ax.set_xlabel('call number $n$', fontsize=fontsize)
    ax.set_ylabel('$\chi^2$ value ', fontsize=fontsize)
    #ax.legend()
    ax.set_xticks(x)
    ax.xaxis.set_major_locator(MultipleLocator(25))
    ax.xaxis.set_minor_locator(MultipleLocator(5))    
    ax.yaxis.set_major_locator(MultipleLocator(5000))
    ax.yaxis.set_minor_locator(MultipleLocator(1000))
    ax.tick_params(axis='both', which='major', labelsize=labelsize)
    ax.grid(alpha = 0.5)
 

#################################################################################
#plot the parameter values of each parameter in each iteration
def plot_convergence_each_param (res,true = None):
    ylabellist = ['$X_0$', '$X_1$', '$X_2$', '$X_3$']
    axlist = [(0,0), (0,1), (1,0), (1,1)]

    for c, axx in enumerate(axlist):
    	p = np.array(res.x_iters)[:,c]
    	ax[axx].plot(list(np.arange(len(p))),p , color = 'xkcd:deep aqua', label ='Parameter Value in call $n$', linewidth = 1.2)
    	if true is not None:
    		ax[axx].axhline(y =true[c], color = 'r', linestyle = '-', label = 'Closure Test Value')
    	ax[axx].set_xlabel('call number $n$', fontsize=fontsize)
    	ax[axx].set_ylabel('Parameter {} in call $n$'.format(ylabellist[c]), fontsize=fontsize)
    	ax[axx].grid(alpha = 0.5)
    	ax[axx].xaxis.set_major_locator(MultipleLocator(25))
    	ax[axx].tick_params(axis='both', which='major', labelsize=labelsize)
    ax[(0,0)].legend(loc ='upper right', fontsize = legendsize)
        

#################################################################################
#plot the partial dependence of the acquisition function on parameter i
#in analogy to partial_dependence_1D of the skopt module
def partial_dependence_1Dacq(space, model, i, samples, y_opt,
                          n_points=40):
    dim_locs = np.cumsum([0] + [d.transformed_size for d in space.dimensions])

    def _calc(x):
        """
        Helper-function to calculate the average predicted
        objective value for the given model, when setting
        the index'th dimension of the search-space to the value x,
        and then averaging over all samples.
        """
        rvs_ = np.array(samples)  # copy
        # We replace the values in the dimension that we want to keep
        # fixed
        rvs_[:, dim_locs[i]:dim_locs[i + 1]] = x
        # In case of `x_eval=None` rvs conists of random samples.
        # Calculating the mean of these samples is how partial dependence
        # is implemented.
        return np.mean(-  _gaussian_acquisition(rvs_, model, y_opt=y_opt, acq_func="EI",return_grad=False, acq_func_kwargs=None))

    xi, xi_transformed = _evenly_sample(space.dimensions[i], n_points)
    # Calculate the partial dependence for all the points.
    yi = [_calc(x) for x in xi_transformed]
    return xi, yi

#plot the partial dependence of the acquisition function on parameters i and j
#in analogy to partial_dependence_2D of the skopt module
def partial_dependence_2Dacq(space, model, i, j, samples,y_opt,
                          n_points=40):
   
    dim_locs = np.cumsum([0] + [d.transformed_size for d in space.dimensions])

    def _calc(x, y):
        """
        Helper-function to calculate the average predicted
        objective value for the given model, when setting
        the index1'th dimension of the search-space to the value x
        and setting the index2'th dimension to the value y,
        and then averaging over all samples.
        """
        rvs_ = np.array(samples)  # copy
        rvs_[:, dim_locs[j]:dim_locs[j + 1]] = x
        rvs_[:, dim_locs[i]:dim_locs[i + 1]] = y
        return np.mean(-  _gaussian_acquisition(rvs_, model, y_opt=y_opt, acq_func="EI",return_grad=False, acq_func_kwargs=None))

    xi, xi_transformed = _evenly_sample(space.dimensions[j], n_points)
    yi, yi_transformed = _evenly_sample(space.dimensions[i], n_points)
    # Calculate the partial dependence for all combinations of these points.
    zi = [[_calc(x, y) for x in xi_transformed] for y in yi_transformed]

    # Convert list-of-list to a numpy array.
    zi = np.array(zi)

    return xi, yi, zi

##plot the partial dependences of the acquisition function on the 1D and 2D parameter subspaces
#in analogy to partial_dependence_1D of the skopt module
#in analogy to plot_objective of the skopt module
def plot_objective_acq(result, levels=10, n_points=40, n_samples=250, size=2,
                   zscale='linear', dimensions=None, sample_source='random',
                   minimum='result', n_minimum_search=None, plot_dims=None,
                   show_points=True, cmap='viridis_r'):

    # Here we define the values for which to plot the red dot (2d plot) and
    # the red dotted line (1d plot).
    # These same values will be used for evaluating the plots when
    # calculating dependence. (Unless partial
    # dependence is to be used instead).
    space = result.space
    # Get the relevant search-space dimensions.
    if plot_dims is None:
        # Get all dimensions.
        plot_dims = []
        for row in range(space.n_dims):
            if space.dimensions[row].is_constant:
                continue
            plot_dims.append((row, space.dimensions[row]))
    else:
        plot_dims = space[plot_dims]
    # Number of search-space dimensions we are using.
    n_dims = len(plot_dims)
    if dimensions is not None:
        assert len(dimensions) == n_dims
   

    x_vals = np.array(result.x)
    x_samples = np.array(result.x_iters)
    if minimum == 'result':
        minimum = x_vals
    elif isinstance(minimum, (list, np.ndarray)):
    	minimum = np.array(minimum)
    
    samples = space.transform(space.rvs(n_samples=n_samples))
    y_opt = np.array(result.fun)

    if zscale == 'log':
        locator = LogLocator()
    elif zscale == 'linear':
        locator = None
    else:
        raise ValueError("Valid values for zscale are 'linear' and 'log',"
                         " not '%s'." % zscale)

    fig, ax = plt.subplots(n_dims, n_dims,
                           figsize=(size * n_dims, size * n_dims))

    fig.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95,
                        hspace=0.1, wspace=0.1)

    for i in range(n_dims):
        for j in range(n_dims):
            if i == j:
                index, dim = plot_dims[i]
                xi, yi = partial_dependence_1Dacq(space, result.models[-1],
                                               index,
                                               samples=samples, y_opt = y_opt,
                                               n_points=n_points)
                print('shape xi', np.shape(xi))
                if n_dims > 1:
                    ax_ = ax[i, i]
                else:
                    ax_ = ax
                ax_.plot(xi, yi)
         
                ax_.axvline(minimum[index], linestyle="--", color="r", lw=1)

            # lower triangle
            elif i > j:
                index1, dim1 = plot_dims[i]
                index2, dim2 = plot_dims[j]
                ax_ = ax[i, j]
                xi, yi, zi = partial_dependence_2Dacq(space, result.models[-1],
                                                   index1, index2,
                                                   samples, y_opt, n_points)
                print('shpae zi', np.shape(zi))
                ax_.contourf(xi, yi, zi, levels,
                             locator=locator, cmap=cmap)
                if show_points:
                    ax_.scatter(x_samples[:, index2], x_samples[:, index1],
                                c='k', s=10, lw=0.)
                ax_.scatter(minimum[index2], minimum[index1],
                            c=['r'], s=100, lw=0., marker='*')
                print('miniminjum index', minimum[index2], minimum[index1])
    ylabel = "Partial dependence"

    # Make various adjustments to the plots.
    return _format_scatter_plot_axes(ax, space, ylabel=ylabel,
                                     plot_dims=plot_dims,
                                     dim_labels=dimensions)

#plot a 2D confidence ellipse for n_std standard deviations of a 2D Gaussian with mean and covariance, eigenvectors v[:,] with eigenvalues w. 
def confidence_ellipse_own(ax,mean, cov, v,w, n_std=1,  **kwargs):  
    mean_x = mean[0]
    mean_y = mean[1]
    angle = np.arctan(v[1,1]/v[0,1])/np.pi*180
    ellipse = Ellipse((mean_x, mean_y), width=np.sqrt(w[1])* 2*n_std, height=np.sqrt(w[0]) * 2*n_std, angle = angle, **kwargs)
    ax.add_patch(ellipse)
    return ellipse     
