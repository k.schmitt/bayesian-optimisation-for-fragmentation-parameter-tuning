from .imports import *
from skopt.plots import _evenly_sample, plot_objective_2D, plot_objective, plot_histogram, plot_evaluations, plot_convergence, _format_scatter_plot_axes,partial_dependence_2D, _evaluate_min_params,_map_categories, plot_gaussian_process
from skopt.acquisition import _gaussian_acquisition
from skopt.learning.gaussian_process.kernels import Matern #as Maternskoptlearning
from skopt.learning.gaussian_process.kernels import ConstantKernel #as ConstantKernelskoptlearning
from skopt.learning.gaussian_process.kernels import WhiteKernel #as WhiteKernelskoptlearning



#calculate covariance between x and y with means xbar and ybar. 
#the weight factor transforms a d-dimensional distribution into d-dim 'scatters' by weighing each point in parameter space
def cov(x, y, weight, xbar, ybar):
        return np.sum((x- xbar)*(y - ybar)*weight)/np.sum(weight)


#calculate the dxd covariance matrix of a nxd dimensional matrix with n points in d dimensional space
def cov_mat(X, weight, mean):
        d = len(X[0])
        co = np.zeros((d,d))
        for i in range(d):
            for j in range(d):
                co[i,j]= cov(X[:,i], X[:,j], weight,mean[i], mean[j])
     
        return co


#predict the optimum value and position of the Gaussian Process and call the event generator with this optimal parameter setting
def predict_after(res, n_points = int(1e4), n_spray = int(1e4), rng= None):
    #####################    
    #fit the last model of the OptimizeResultObject again to the total training set of the optimisation
    tobefit =  np.array(res.space.transform(np.array(res.x_iters)))
    model = res.models[-1].fit(tobefit,np.array(res.func_vals).tolist())
    
    ############################################################################
    #predict GP on n_points points:
    Xp = res.space.transform(res.space.rvs( n_samples=n_points, random_state=rng)) #this is in warped space!
    
    #hack: obtain spray points around the best blackbox function value found so far
    spray_std = int(1e-1) # 1% of search space
    spray_points = np.random.randn(n_spray, res.space.n_dims)*spray_std +  res.space.transform([res.x])
    #(random number between 0 and 1)* standard dev of the spray points + current best point called
    
    spray_points = np.minimum(np.maximum(spray_points,0.0),1.0)
    Xp = np.vstack((Xp, spray_points))
    Xpreal = np.array(res.space.inverse_transform(Xp))

 
    y_pred, sigma = model.predict(Xp, return_std=True)
    ybest=min(y_pred)
    best=np.argmin(y_pred)
    xbest = Xpreal[best]   
    
    values = -_gaussian_acquisition(Xp, model, y_opt=float(res.fun), acq_func='EI' ,return_grad=False, acq_func_kwargs=None)
    maxacq = Xpreal[np.argmax(values)]

    return ybest, xbest, maxacq
 
    
#calculate the uncertainty by sampling from the Gaussian Process as described in the thesis
def uncertainty_sampling(res,n_points = int(1e3),n_spray = int(1e3), samplesperbatch = 1, batchnum = 100,xbestpred = None):
    ###################################################
    #error estimation using sampling
    #problem: need cov matrix of GP -> takes up a lot of memory for too many points
    #solution for now: only use 1e3 points + 1e3 spray points around best optimum found 
    #but use randomly generated different points for each sampling, 
    #with the possibility to sample in batches                    
    tobefit =  np.array(res.space.transform(np.array(res.x_iters)))
    model = res.models[-1].fit(tobefit,np.array(res.func_vals).tolist()) #just to make sure: fit model again!
    n_dims = res.space.n_dims
    
    seedsample = 1
    rngsample = check_random_state(seedsample)
    np.random.seed(seedsample)

    spray_std1 = 1e-2
    spray_std2= 1e-2
    spray_points1 = np.random.randn(n_spray, n_dims)*spray_std1 + res.space.transform([res.x])
     # (random number between 0 and 1)* standard dev of the spray points + current best point called
    spray_points2 = np.random.randn(n_spray, n_dims)*spray_std2 + res.space.transform(xbestpred)
       # (random number between 0 and 1)* standard dev of the spray points + best predicted
    spray_points1 = np.minimum(np.maximum(spray_points1,0.0),1.0) 
    spray_points2 = np.minimum(np.maximum(spray_points2,0.0),1.0) 
       
    Xps = res.space.transform(res.space.rvs( n_samples=n_points, random_state=rngsample))
    Xps = np.vstack((Xps, spray_points1))
    Xps = np.vstack((Xps, spray_points2))
    
    Xps_real = np.array(res.space.inverse_transform(Xps))
    y_ps, cov_ps= model.predict(Xps, return_cov=True)
    y_ps, std_ps= model.predict(Xps, return_std=True)

    y_ps = y_ps.reshape(-1)
        
    values = -_gaussian_acquisition(Xps, model, y_opt=float(res.fun), acq_func='PI' ,return_grad=False, acq_func_kwargs=None)

    def minns():
        min_ps = Xps_real[np.argmin(np.random.multivariate_normal(y_ps, cov_ps))]
        return min_ps
  
    indiceslist = Parallel(n_jobs=4)(delayed(minns) for i in range(samplenum))
    indiceslist = np.array(indiceslist).reshape((-1, n_dims))
    
    #now treat the positions of the predicted optima as obeying a Gaussian distribution in parameter space                    
    mean_s = np.mean(indiceslist, axis= 0)
    cov_s = cov_mat(indiceslist, np.ones(len(indiceslist),), mean_s)
    print('indiceslist', indiceslist)
    print('maen cov', mean_s,'\n', cov_s)
    w_s, v_s = np.linalg.eig(cov_s)
    idx_s = w_s.argsort()
    w_s = w_s[idx_s]
    v_s = v_s[:,idx_s]
 
    replicapoints = []
    for c in range(len(w_s)):
            replicapoints.append(mean_s + np.dot( np.sqrt(w_s[c]*scipy.stats.chi2.isf(0.05,df =4)) , v_s[:,c]))
            replicapoints.append(mean_s - np.dot( np.sqrt(w_s[c]*scipy.stats.chi2.isf(0.05,df =4)) , v_s[:,c]))

    replicapoints = np.array(replicapoints)
    print(replicapoints)
    chisqu_pred_repl_samp = model.predict(res.space.transform(replicapoints.reshape(-1, n_dims)))
    print('\n', chisqu_pred_repl_samp)
    return replicapoints, chisqu_pred_repl_samp

   

def create_result_object(name,space,rng):
    df = pd.read_csv(name, sep="\t", header = None, skiprows = 7)#, comment = '#')
    yi = np.array(df[0].astype(float)).reshape(-1,)  
    x = {}
    werte = np.array( df[1])
    for c in range(4):
    	x[c] = np.array( [float(elm.strip('[]').split(',')[c]) for elm in  werte]).reshape(-1,1)
    Xi = np.hstack((x[0], x[1], x[2],x[3]))
    tobefit =  np.array(space.transform(np.array(Xi)))
    model = cook_estimator('GP',space=space)
    model = model.fit(tobefit, yi)
    print('model', model, type(model), model.kernel_)
    res = create_result(Xi, yi, space, rng,  models=[model])
    return res
    

