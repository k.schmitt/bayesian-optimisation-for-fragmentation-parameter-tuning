from .imports import *

################################################################################

def write_res_full(name, res,  ybest, xbest, maxacq,replicas_samp,chisqu_pred_repl_samp, chisqu_real_repl_samp ,n_samples, params_true= None):
	print('name', name, ' win writing')
	with open(name, 'a') as file:
		file.write('\n Best found : '+ str(res.x))
		file.write('\n Best predicted : ' + str(xbest) + ' with chisquare ' + str(ybest))
		file.write('\n Next point called would be according to acquisition function: ' + str(maxacq))
		file.write('\n Replicas Sampling with ' + str(n_samples) + ': \n')
		for i in range(len(chisqu_real_repl_samp)):
			file.write(str(replicas_samp[i]) + ' with predicted chisquare ' + str(chisqu_pred_repl_samp[i]) + ' and real chisquare ' + str(chisqu_real_repl_samp[i]))
		file.write('\n')

def write_res(res, name = 'resskopt.txt'):
    with open(name, 'a') as file:
        now = datetime.now()

        file.write('# '+str(now) +'\n')
        file.write(str(res))
################################################################################

def write_results_each_it_grid(i,p, res, resdof, name = 'results_each_it_grid.txt'):
    with open(name, 'a') as file:
        file.write(str(i) + ' \t'+str(res)+ '\t'+ str(resdof) + '\t'+str(p[0])+'\t'+ str(p[1])+'\t'+ str(p[2])+ '\t'+str(p[3])+'\n')

################################################################################################

def callres(res,name ):
	#print('here')
	#name = 'test.txt'
	with open(name, 'a') as file:
		file.write(str(res.func_vals[-1]) + '\t' + str(res.x_iters[-1]) + '\t' + str(res.models[-1].kernel_ )+'\n')
		
		
def calltime(res, name): 
	with open(name, 'a') as file:
		file.write(str(time()) + '\t' + str(process_time())+ '\n')
