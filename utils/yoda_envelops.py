#!/usr/bin/env python

"""

Based on Enrico Bothmann's yoda-envelop script
adapted by Max Knobbe to suit tuning needs

"""


import yoda
import sys

def print_usage_and_exit():
    print("Usage: $0 -o <outfile> <infiles>")
    sys.exit(1)

if sys.argv[1] != "-o" or len(sys.argv) < 4:
    print_usage_and_exit()

out_file = sys.argv[2]
in_files = sys.argv[3:]

data_object_lists = []
for path in in_files:
    data_object_lists.append(yoda.readYODA(path))

new_data_objects = []

for key, obj in data_object_lists[0].items():
    new_data_object = obj.mkScatter()

    # check if data_object for key exists in all in_files, otherwise ignore
    # this data_object
    should_ignore = False
    for data_objects in data_object_lists[1:]:
        if not key in data_objects:
            should_ignore = True
            break
    if should_ignore:
        continue

    for i, p in enumerate(new_data_object.points()):
        if p.dim() == 1:
            low  = p.x() - p.xErrs()[0]
            high = p.x() + p.xErrs()[1]
        elif p.dim() == 2:
            low  = p.y() - p.yErrs()[0]
            high = p.y() + p.yErrs()[1]
        for data_objects in data_object_lists[1:]:
            var_p = data_objects[key].mkScatter().points()[i]
            var_low  = var_p.x() - p.xErrs()[0] if var_p.dim() == 1 else var_p.y() - p.yErrs()[0]
            var_high = var_p.x() + p.xErrs()[1] if var_p.dim() == 1 else var_p.y() + p.yErrs()[1]
            low  = min(low, var_low)
            high = max(high, var_high)
        if p.dim() == 1:
            p.setErrs(p.dim(), p.x() - low, high - p.x())
        elif p.dim() == 2:
            p.setErrs(p.dim(), p.y() - low, high - p.y())
    new_data_objects.append(new_data_object)

yoda.writeYODA(new_data_objects, out_file)
